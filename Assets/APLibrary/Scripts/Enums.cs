﻿public enum LoadSceneType {

    LOAD_BY_NAME,
    LOAD_BY_INDEX
}

public enum GameState
{
    NONE, // This is just a nothing to do state but activate next state GAME_DATA_LOADED
    GAME_DATA_LOADED, // Game wii start loading gameplay data in this state, this state will call once Scen opend
    GAME_INITIALIZED, // After loading gameplay data this state will be called, Game will be ready to play
    GAME_PLAY_STARTED,
    GAME_PLAY_ENDED,
    GAMEPLAY_PAUSE,
    GAMEPLAY_UNPAUSE
}
