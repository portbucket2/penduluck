﻿/*
 * Developer E-mail: sandsoftimer@gmail.com
 * Facebook Account: https://www.facebook.com/md.imran.hossain.902
 * This is a manager which will give common functional supports. 
 * like, WrapAngle, UnwrapAngle etc.
 *  
 */

using UnityEngine;

public class MathManager : MonoBehaviour
{
    public float WrapAngle(float angle)
    {
        angle %= 360;
        return angle = angle > 180 ? angle - 360 : angle;
    }

    public float UnwrapAngle(float angle)
    {
        if (angle >= 0)
            return angle;

        angle = -angle % 360;

        return 360 - angle;
    }
}
