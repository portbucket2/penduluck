﻿using UnityEditor;
using UnityEngine;

public class APEditor : Editor
{
    protected void DrawHorizontalLine()
    {
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
    }

    protected void DrawHorizontalLineOnGUI(Rect rect)
    {
        EditorGUI.LabelField(rect, "", GUI.skin.horizontalSlider);
    }

}
