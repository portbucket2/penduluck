﻿using UnityEngine;
using com.alphapotato.utility;

[DefaultExecutionOrder(ConstantManager.APBehaviourOrder)]
public class APBehaviour : MonoBehaviour
{
    [HideInInspector]
    public APManager gameManager;
    [HideInInspector]
    public APTools APTools;

    [HideInInspector]
    public GameplayData gameplayData;
    [HideInInspector]
    public GameState gameState;

    //[HideInInspector]
    public GameMechanism gameMechanism;

    public virtual void Awake()
    {
        APTools = APTools.Instance;
    }

    public virtual void OnEnable()
    {
        APManager.OnSelfDistributionAction += OnSelfDistributionAction;
        APManager.OnNone += OnNone;
        APManager.OnGameDataLoad += OnIdle;
        APManager.OnGameInitialize += OnGameInitializing;
        APManager.OnGameStart += OnGameStart;
        APManager.OnGameOver += OnGameOver;
        APManager.OnChangeGameState += OnChangeGameState;
        APManager.OnCompleteTask += OnCompleteTask;
        APManager.OnIncompleteTask += OnIncompleteTask;

        GameManager.OnReadyToJump += OnReadyToJump;
        GameManager.OnReadyToFollow += OnReadyToFollow;
    }

    public virtual void OnDisable()
    {
        APManager.OnSelfDistributionAction -= OnSelfDistributionAction;
        APManager.OnNone -= OnNone;
        APManager.OnGameDataLoad -= OnIdle;
        APManager.OnGameInitialize -= OnGameInitializing;
        APManager.OnGameStart -= OnGameStart;
        APManager.OnGameOver -= OnGameOver;
        APManager.OnChangeGameState -= OnChangeGameState;
        APManager.OnCompleteTask -= OnCompleteTask;
        APManager.OnIncompleteTask -= OnIncompleteTask;

        GameManager.OnReadyToJump -= OnReadyToJump;
        GameManager.OnReadyToFollow -= OnReadyToFollow;
    }

    public virtual void Start()
    {
        APManager.OnAddAPBehaviour?.Invoke();
    }

    public virtual void OnReadyToJump()
    {
    }

    public virtual void OnReadyToFollow()
    {
    }

    public virtual void OnSelfDistributionAction(APManager aPManager)
    {
        gameManager = aPManager;
        gameplayData = aPManager.gameplayData;
        gameState = aPManager.gameState;
        gameMechanism = ((GameManager)gameManager).gameMechanism;
    }

    public virtual void OnChangeGameState(GameState gameState)
    {
        this.gameState = gameState;
    }

    public virtual void OnIncompleteTask()
    {
    }

    public virtual void OnNone()
    {
    }

    public virtual void OnIdle()
    {
    }

    public virtual void OnGameInitializing()
    {        
    }

    public virtual void OnGameStart()
    {        
    }

    public virtual void OnGameOver()
    {
    }

    public virtual void OnCompleteTask()
    {
    }

}
