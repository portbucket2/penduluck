﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Obsolete]
public class APConfigarator : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        #region Debug log for anything happend on asset folder
        //foreach (string str in importedAssets)
        //{
        //    Debug.Log("Reimported Asset: " + str);
        //}
        //foreach (string str in deletedAssets)
        //{
        //    Debug.Log("Deleted Asset: " + str);
        //}

        //for (int i = 0; i < movedAssets.Length; i++)
        //{
        //    Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
        //}
        #endregion Debug log for anything happend on asset folder

        SetUnityLayers();
        //ReAssagineList();
        SetEditorBuildSettingsForAP();
    }

    public static void SetUnityLayers()
    {
        #region Setup Unity Layers & Tags
        SerializedObject manager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
        SerializedProperty layersProp = manager.FindProperty("layers");
        string[] layersName = ConstantManager.layerNames.Values.ToArray();

        bool layerMismatchfound = false;
        for (int i = 0; i < layersName.Length; i++)
        {
            SerializedProperty sp = layersProp.GetArrayElementAtIndex(i);
            if (!layersName[i].Equals(sp.stringValue))
            {
                layerMismatchfound = true;
                Debug.LogError("Unity Layer Names has been <Color=red>Repaired</Color>. To modify/add layers, see layerNames in <Color=Yellow>ConstantManager.cs</Color>");
                break;
            }
        }

        if (layerMismatchfound)
        {
            for (int i = 8; i < layersName.Length; i++)
            {
                SerializedProperty layerSP = layersProp.GetArrayElementAtIndex(i);
                layerSP.stringValue = layersName[i];
                manager.ApplyModifiedProperties();
            }
        }
        #endregion Setup Unity Layers
    }

    public static void SetEditorBuildSettingsForAP()
    {
        #region Setup BuildSetting
        int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
        string[] scenesPath = new string[sceneCount];
        List<string> newScenesPath = new List<string>();
        int bootSceneIndex = -1;
        for (int i = 0; i < sceneCount; i++)
        {
            scenesPath[i] = UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i);
            if (scenesPath[i].Contains("BootScene"))
            {
                bootSceneIndex = i;
            }
        }

        bool flag = false;
        if (bootSceneIndex == -1)
        {
            // Boot Scene not added yet.
            // Adding Boot Scene to BuildIndex
            Debug.LogError("<Color=red>BootScene was not found in BuildSettings.</Color> <Color=green>Re-Added at 0 index</Color>");
            flag = true;
            string bootScenePath = "Assets/APLibrary/Scenes/BootScene.unity";

            //string[] newScenesPath = new string[scenesPath.Length + 1];
            for (int i = 0; i < scenesPath.Length + 1; i++)
            {
                if (i == 0)
                    newScenesPath.Add(bootScenePath);
                else
                    newScenesPath.Add(scenesPath[i -1]);
            }
        }
        else if(bootSceneIndex != 0)
        {
            // Boot Scene must be at 0 index.
            Debug.LogError("<Color=green>BuildSettings Updated.</Color> <Color=yellow>BootScene must be at 0 index.</Color>");
            flag = true;

            List<string> tempScenesPath = new List<string>();
            tempScenesPath = scenesPath.ToList();

            newScenesPath.Add(scenesPath[bootSceneIndex]);
            for (int i = 0; i < tempScenesPath.Count; i++)
            {
                if (i != bootSceneIndex)
                    newScenesPath.Add(scenesPath[i]);
            }
        }

        if (flag)
        {
            EditorBuildSettingsScene[] editorBuildSettingsScene = new EditorBuildSettingsScene[newScenesPath.Count];
            for (int j = 0; j < newScenesPath.Count; j++)
            {
                EditorBuildSettingsScene sceneToAdd = new EditorBuildSettingsScene(newScenesPath[j], true);
                editorBuildSettingsScene[j] = sceneToAdd;
            }
            EditorBuildSettings.scenes = new EditorBuildSettingsScene[0];
            EditorBuildSettings.scenes = editorBuildSettingsScene;
        }
        #endregion Setup BuildSetting
    }
}
#endif