using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody))]
public class MovingPlatform : APBehaviour
{
    public Renderer cubeRenderer;
    public Material movingPlatformMaterial;
    public PlatformType platformType;
    public Ease easeType;
    public float moveLimit, timeToFinish;

    Rigidbody rig;
    Vector3 startingPosition;
    int side;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        startingPosition = transform.position;
        side = 1;

        rig = GetComponent<Rigidbody>();
            rig.isKinematic = true;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        if (cubeRenderer)
        {
            if (!platformType.Equals(PlatformType.NONE))
            {
                cubeRenderer.material = movingPlatformMaterial;
            }
        }

        Execute();
    }

    private void FixedUpdate()
    {
        //if(transform.position.x + side)
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameOver()
    {
        base.OnGameOver();
        DOTween.Clear(transform);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    void Execute()
    {
        switch (platformType)
        {
            case PlatformType.NONE:

                break;
            case PlatformType.MOVE_X:
                transform.DOMove(
                    new Vector3(startingPosition.x + moveLimit * side, transform.position.y, transform.position.z), timeToFinish).SetEase(easeType).OnComplete(() => {
                        side *= -1;
                        Execute();
                    });
                break;
            case PlatformType.MOVE_Y:
                transform.DOMove(
                    new Vector3(transform.position.x, startingPosition.y + moveLimit * side, transform.position.z), timeToFinish).SetEase(easeType).OnComplete(() => {
                        side *= -1;
                        Execute();
                    });
                break;
            case PlatformType.MOVE_Z:
                transform.DOMove(
                    new Vector3(transform.position.x, transform.position.y, startingPosition.z + moveLimit * side), timeToFinish).SetEase(easeType).OnComplete(() => {
                        side *= -1;
                        Execute();
                    });
                break;
            case PlatformType.BROKEN:
                transform.DOMove(startingPosition, timeToFinish).SetEase(easeType).OnComplete(() => {
                    Destroy(gameObject);
                });
                break;
        }
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
