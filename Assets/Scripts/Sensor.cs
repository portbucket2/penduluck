using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : APBehaviour
{
    public List<Transform> sensorUserList;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        sensorUserList = new List<Transform>();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        //gameManager.totalGivenTask++;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void AddThisObject(Transform obj)
    {
        sensorUserList.Add(obj);
    }

    public bool IsThisObjectAlreadyUsedThisSensor(Transform match)
    {
        return sensorUserList.Contains(match);
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
