using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : APManager
{
    public GameMechanism gameMechanism;
    public TextMeshProUGUI totalGhangoPango;

    public static event Action OnReadyToJump;
    public static event Action OnReadyToFollow;

    bool gameTutorialTextShown;

    public void ShowScore(int numberOfShnagoPango)
    {
        totalGhangoPango.text = (numberOfShnagoPango * 100) + " Points";
    }

    public void ReadyToJump()
    {
        OnReadyToJump?.Invoke();

        if (!gameTutorialTextShown)
        {
            gameTutorialTextShown = true;
            gameStartingUI.SetBool("Hide", false);
        }

    }

    public void ReadyToFollow()
    {
        OnReadyToFollow?.Invoke();
        gameStartingUI.SetBool("Hide", true);
    }

    public override void Awake()
    {
        base.Awake();
        ChangeGameState(GameState.NONE);
    }

    public override void None()
    {
        base.None();
        ChangeGameState(GameState.GAME_DATA_LOADED);

    }

    public override void GameDataLoad()
    {
        base.GameDataLoad();
        ChangeGameState(GameState.GAME_INITIALIZED);
    }

    public override void GameInitialize()
    {
        base.GameInitialize();
        ChangeGameState(GameState.GAME_PLAY_STARTED);

    }

    public override void GameStart()
    {
        base.GameStart();
        gamePlayUI.SetBool("Hide", false);

        LionStudios.Analytics.Events.LevelStarted((gameplayData.currentLevelNumber + 1));

    }

    public override void GameOver()
    {
        base.GameOver();
        gameStartingUI.SetBool("Hide", true);

        if (gameplayData.isGameoverSuccess)
        {
            LionStudios.Analytics.Events.LevelComplete(gameplayData.currentLevelNumber);
        }
        else
        {
            LionStudios.Analytics.Events.LevelFailed((gameplayData.currentLevelNumber + 1));
        }
    }
}
