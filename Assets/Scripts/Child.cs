using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Rigidbody))]
public class Child : APBehaviour
{
    public AnimationCurve animationCurve;
    public Vector3 followOffset;
    public Collider sensorCollider;
    public Transform selfStackPoint;
    public Child follower, following;
    public Transform groundCheckPoint;
    public float groundCheckDistance;

    public bool readyToFollow, isBusyToJump, isInPendulum, isGrounded;
    Animator anim;
    Rigidbody rig;
    PlayerController playerController;
    Vector3 lastPostion;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        anim = GetComponent<Animator>();
        rig = GetComponent<Rigidbody>();
        lastPostion = transform.position;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        if (groundCheckPoint)
        {
            RaycastHit hit = new RaycastHit();
            bool isGrounded = Physics.Raycast(groundCheckPoint.position, -transform.up, out hit, groundCheckDistance, 1 << ConstantManager.GROUND_LAYER);
            if (!this.isGrounded && isGrounded)
            {
                transform.parent = hit.transform;
                transform.localEulerAngles = Vector3.zero;
                isBusyToJump = false;
                sensorCollider.enabled = false;
                sensorCollider.enabled = true;
                rig.velocity = Vector3.zero;
                //readyToFollow = true;
            }
            this.isGrounded = isGrounded;

            bool isInPendulum = Physics.Raycast(groundCheckPoint.position, -transform.up, out hit, groundCheckDistance, 1 << ConstantManager.PENDULUM_LAYER);
            if(!this.isInPendulum && isInPendulum)
            {
                sensorCollider.enabled = false;
                sensorCollider.enabled = true;
            }
            this.isInPendulum = isInPendulum;

            if (isGrounded || isInPendulum)
            {
                anim.SetBool("IsGrounded", true);
                anim.SetBool("IsInPendulum", true);

                ((CapsuleCollider)sensorCollider).height = 50;
                ((CapsuleCollider)sensorCollider).center = new Vector3(0.2024232f, 25, 0f);
            }
            else
            {
                anim.SetBool("IsGrounded", false);
                anim.SetBool("IsInPendulum", false);

                ((CapsuleCollider)sensorCollider).height = 2;
                ((CapsuleCollider)sensorCollider).center = new Vector3(0.2024232f, 1, 0f);
            }
        }
        
    }

    private void LateUpdate()
    {        
        lastPostion = transform.position;
    }

    public void FixedUpdate()
    {
        if (!following)
            return;

        if (!isBusyToJump && !isInPendulum)
        {
            Vector3 newPosition = Vector3.Lerp(
                transform.position,
                following.transform.position + followOffset,
                1f);
            if (newPosition.x < transform.position.x)
                newPosition.x = transform.position.x;
            newPosition.z = transform.position.z;
            newPosition.y = transform.position.y;

            transform.position = newPosition;
            //rig.position = newPosition;
            anim.SetFloat("HorizontalSpeed", transform.position.x - lastPostion.x >= 0.01f ? 1f : 0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {
            IamLeavingFromChain();
            GetComponent<Collider>().enabled = false;
            gameplayData.isGameoverSuccess = false;
            gameManager.ChangeGameState(GameState.GAME_PLAY_ENDED);
        }

        if (other.gameObject.layer.Equals(ConstantManager.SENSOR))
        {
            Sensor sensor = other.GetComponent<Sensor>();
            if (other.CompareTag("JumpingPoint"))
            {
            }
            else if (other.CompareTag("PendulumJumpingPoint") && !sensor.IsThisObjectAlreadyUsedThisSensor(transform))
            {
                sensor.AddThisObject(transform);
                isInPendulum = true;
                rig.isKinematic = true;

                transform.localEulerAngles = Vector3.zero;
                ParabolaMove parabolaMove = transform.gameObject.AddComponent<ParabolaMove>();
                parabolaMove.Initialized(
                    animationCurve,
                    other.transform,
                    0f,
                    ConstantManager.DEFAULT_ANIMATION_TIME / 5f,
                    false,
                    true,
                    () =>
                    {
                        transform.parent = other.transform;
                        transform.localPosition = Vector3.zero;
                        transform.localEulerAngles = Vector3.zero;
                        //IamLeavingFromChain();
                        isInPendulum = true;
                        isBusyToJump = false;
                        readyToFollow = true;
                    });
            }

        }
    }


    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameStart()
    {
        base.OnGameStart();
    }

    public override void OnReadyToFollow()
    {
        base.OnReadyToFollow();
        //readyToFollow = true;
    }

    public override void OnReadyToJump()
    {
        base.OnReadyToJump();
        //readyToFollow = false;
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(Child child, PlayerController playerController, bool readyToFollow)
    {
        this.playerController = playerController;
        following = child;
        child.follower = this;
        this.readyToFollow = true;
    }

    public void JumpFromHere(Vector3 reachingPoint, float speed, float jumpForce)
    {
        isBusyToJump = true;
        readyToFollow = false;
        rig.useGravity = true;
        rig.isKinematic = false;
        transform.parent = null;
        float timeToReach = Vector3.Distance(transform.position, reachingPoint) / speed;
        transform.DOMove(reachingPoint, 0).OnComplete(()=> {

            rig.velocity = new Vector3(speed, 0, 0);
            rig.AddForce(new Vector3(0, jumpForce, 0));
        });
        IamLeavingFromChain();
    }

    public void IamLeavingFromChain()
    {
        //Debug.LogError("Disconnecting......");
        if(playerController)
            playerController.ClearThisChild(this);
        if(follower)
            follower.Initialize(following, playerController, readyToFollow);
        follower = null;
        following = null;
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
