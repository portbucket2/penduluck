using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Animator))]
public class PlayerController : APBehaviour
{
    public AnimationCurve animationCurve;
    public AudioClip jumpClip;
    public Animator headCanvasAnim;
    public Transform groundCheckPoint;
    public float groundCheckDistance;
    public float runningSpeed;
    public float jumpForceY;
    public bool isGrounded, isInPendulum;
    public bool readyToJump;
    public bool coroutineFromChild;
    public int jumpCounter;
    public Child defaultChild, lastChild;
    public List<Child> followers;

    Rigidbody rig;
    Animator anim;
    public Vector3 velocity;
    float cameraRunningPosition = 60f, cameraWaitingPositino = -30f, cameraCurrentPosition, cameraTransitionSpeed = 10f;
    Coroutine processChildrensToJump;
    bool jumpTime;
    PlayerFollowCam playerFollowCam;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = 50;
        rig = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        jumpCounter = 0;
        anim.SetBool("IsGrounded", isGrounded);
        anim.SetBool("ReadyToJump", true);
        cameraRunningPosition = Camera.main.transform.position.z;
        cameraCurrentPosition = cameraRunningPosition;
        followers = new List<Child>() { defaultChild };
        playerFollowCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerFollowCam>();
        //headCanvasAnim.transform.LookAt(-Camera.main.transform.position);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        RaycastHit hit = new RaycastHit();
        bool isGrounded = Physics.Raycast(groundCheckPoint.position, -transform.up, out hit, groundCheckDistance, 1 << ConstantManager.GROUND_LAYER);
        if (!this.isGrounded && isGrounded)
        {
            jumpCounter = 0;
            transform.parent = hit.transform;
            transform.localEulerAngles = Vector3.zero;
            //((GameManager)gameManager).ReadyToFollow();

        }
        this.isGrounded = isGrounded;

        isInPendulum = Physics.Raycast(groundCheckPoint.position, -transform.up, out hit, groundCheckDistance, 1 << ConstantManager.PENDULUM_LAYER);

        if (isGrounded || isInPendulum)
        {
            anim.SetBool("IsGrounded", true);
            //anim.SetBool("IsInPendulum", true);
        }
        else
        {
            anim.SetBool("IsGrounded", false);
            //anim.SetBool("IsInPendulum", false);
        }

        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        anim.SetFloat("VerticalSpeed", rig.velocity.y);
        anim.SetBool("ReadyToJump", readyToJump);

        if (Input.GetMouseButtonDown(0) && readyToJump)
        {
            gameManager.gameStartingUI.SetBool("Hide", true);
            coroutineFromChild = false;
            processChildrensToJump = StartCoroutine(ProcessChildrensToJump());
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (processChildrensToJump != null)
                StopCoroutine(processChildrensToJump);
        }

        if (readyToJump)
        {
            velocity.x = 0;
        }
        else
            velocity.x = runningSpeed;

    }

    private void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        velocity.y = rig.velocity.y;
        rig.velocity = velocity;

        //Camera.main.transform.position =
        //    new Vector3(
        //        Camera.main.transform.position.x,
        //        Camera.main.transform.position.y,
        //        Mathf.Lerp(Camera.main.transform.position.z, cameraCurrentPosition, Time.deltaTime * cameraTransitionSpeed));
    }

    IEnumerator ProcessChildrensToJump()
    {
        if (followers.Count > 1)
        {
            yield return new WaitForSeconds(0.15f);
            Child child = followers[1];
            child.JumpFromHere(transform.position, runningSpeed, jumpForceY);
            gameManager.PlayThisSoundEffect(jumpClip);

            coroutineFromChild = true;
            processChildrensToJump = StartCoroutine(ProcessChildrensToJump());
        }
        else
        {
            yield return new WaitForSeconds(0.30f);
            rig.isKinematic = false;
            transform.localEulerAngles = Vector3.zero;
            jumpTime = true;
            readyToJump = false;
            jumpCounter++;
            transform.parent = null;

            rig.AddForce(0, jumpForceY, 0, ForceMode.Acceleration);
            gameManager.PlayThisSoundEffect(jumpClip);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (other.gameObject.layer.Equals(ConstantManager.PICKUPS_LAYER))
        {
            Child child = other.transform.parent.GetComponent<Child>();

            if (!followers.Contains(child) && !child.isBusyToJump)
            {
                child.Initialize(lastChild, this, !readyToJump);
                lastChild = child;
                followers.Add(child);
                //headCanvasAnim.SetTrigger("ShowPoints");
            }
        }

        if (other.gameObject.layer.Equals(ConstantManager.SENSOR))
        {
            Sensor sensor = other.GetComponent<Sensor>();
            if (other.CompareTag("JumpingPoint"))
            {
                other.enabled = false;
                rig.velocity = Vector3.zero;
                other.gameObject.SetActive(false);
                ((GameManager)gameManager).ReadyToJump();
            }
            else if (other.CompareTag("PendulumJumpingPoint") && !sensor.IsThisObjectAlreadyUsedThisSensor(transform))
            {
                sensor.AddThisObject(transform);
                rig.isKinematic = true;

                transform.localEulerAngles = Vector3.zero;
                ParabolaMove parabolaMove = transform.gameObject.AddComponent<ParabolaMove>();
                parabolaMove.Initialized(
                    animationCurve,
                    other.transform,
                    0f,
                    ConstantManager.DEFAULT_ANIMATION_TIME / 5f,
                    false,
                    true,
                    () => {

                        transform.parent = other.transform;
                        transform.localPosition = Vector3.zero;
                        transform.localEulerAngles = Vector3.zero;
                        ((GameManager)gameManager).ReadyToJump();
                    });
            }
        }

        if (other.gameObject.layer.Equals(ConstantManager.DESTINATION_LAYER))
        {
            ((GameManager)gameManager).ReadyToJump();

            ((GameManager)gameManager).ShowScore(followers.Count - 1);
            gameManager.OnCompleteATask();
        }

        if (other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {
            gameplayData.isGameoverSuccess = false;
            gameManager.ChangeGameState(GameState.GAME_PLAY_ENDED);
        }


    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnReadyToFollow()
    {
        base.OnReadyToFollow();
        cameraCurrentPosition = cameraRunningPosition;
        //playerFollowCam.followOffset.z = cameraRunningPosition;
        readyToJump = false;
    }

    public override void OnReadyToJump()
    {
        base.OnReadyToJump();
        cameraCurrentPosition = cameraWaitingPositino;
        //playerFollowCam.followOffset.z = cameraWaitingPositino;
        readyToJump = true;
    }

    public override void OnGameStart()
    {
        base.OnGameStart();
        gameManager.totalGivenTask = 1;
    }

    public override void OnGameOver()
    {
        base.OnGameOver();
        rig.velocity = Vector3.zero;
        if (gameplayData.isGameoverSuccess)
        {
            //GetComponent<Collider>().enabled = false;
            anim.SetTrigger("Celebrate");
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void ClearThisChild(Child child)
    {
        followers.Remove(child);
        lastChild = followers[followers.Count - 1];
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
