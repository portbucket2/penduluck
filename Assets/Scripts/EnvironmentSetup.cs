using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSetup : APBehaviour
{
    public float xOffset, yOffset, zOffset;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform aBuilding = transform.GetChild(i);

            float x = aBuilding.position.x + Random.Range(-xOffset, xOffset);
            float y = aBuilding.position.y + Random.Range(-yOffset, yOffset);
            float z = aBuilding.position.z + Random.Range(-zOffset, zOffset);
            aBuilding.position = new Vector3(x, y, z);

            aBuilding.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
    
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
