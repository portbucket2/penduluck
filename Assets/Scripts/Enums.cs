public enum PlatformType
{
    NONE,
    MOVE_X,
    MOVE_Y,
    MOVE_Z,
    BROKEN
}

public enum GameMechanism
{
    CHILD_FOLLOW_STACK,
    CHILD_JUMP_AFTER,
    CHILD_JUMP_BEFORE
}