using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Random = UnityEngine.Random;

public class Pendulum : APBehaviour
{
    public Ease easeType;
    public Transform hook, playerSnappingPoint;
    public float angleLimit, speed;
    float currentAngle;
    int side;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        side = 1;
        hook.transform.eulerAngles = new Vector3(0, 0, angleLimit * -side);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        Rotate(Random.Range(angleLimit - 10, angleLimit));
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    void Rotate(float angle)
    {
        hook.transform.DORotate(
            new Vector3(0, 0, angle * side), speed).SetEase(easeType).OnComplete(()=> {

                side *= -1;
                //Rotate( Random.Range(angleLimit - 10, angleLimit));
                Rotate(angleLimit);
            });

    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
